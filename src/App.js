import {
    faAngleUp,
    faBars,
    faCopyright,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
import { useEffect } from 'react';
import SwiperCore, {
    A11y,
    Navigation,
    Pagination,
    Scrollbar,
    Zoom,
} from 'swiper';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
// Import Swiper styles
import 'swiper/swiper.scss';
import './App.css';
import About from './components/about';
import Contact from './components/contact';
import Home from './components/home';
import Project from './components/project';
import Service from './components/service';
import Skill from './components/skill';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Zoom]);
AOS.init({ once: true });
function App() {
    useEffect(() => {
        const navItems = document.querySelectorAll('.menu-item-link');
        const sections = document.querySelectorAll('section');
        const observe = new IntersectionObserver((item) => {
            if (item[0].isIntersecting) {
                item[0] && navItems.forEach(i => {
                    if (i.getAttribute('href').includes(item[0].target.getAttribute('id'))) {
                        i.classList.add('active')
                    }
                    else {
                        i.classList.remove('active')
                    }
                })
            }
        }, { threshold: 0.5 })
        sections.forEach(item => observe.observe(item))
        window.addEventListener(
            'scroll',
            function (e) {
                const nav = document.getElementById('navbar');
                if (window.scrollY > 0) {
                    nav.classList.add('sticky');
                } else {
                    nav.classList.remove('sticky');
                }
                const scrollUp = document.getElementById('scroll-up-btn');
                if (window.scrollY > 500) {
                    scrollUp.classList.add('show');
                } else {
                    scrollUp.classList.remove('show');
                }

                //nav.classList
                // sections.forEach((section) => {
                //   // 50 is height of nav
                //   const top = section.offsetTop - 300;
                //   const html = document.documentElement;
                //   const height = section.offsetHeight;

                //   navItems.forEach((navItem) => {
                //     const hrefNav = navItem.href.match(/#[a-zA-Z]+/)[0];
                //     const idSection = '#' + section.id;

                //     if (
                //       html.scrollTop >= top &&
                //       top + height >= html.scrollTop
                //     ) {
                //       hrefNav === idSection
                //         ? navItem.classList.add('active')
                //         : navItem.classList.remove('active');
                //     } else {
                //       hrefNav === idSection &&
                //         navItem.classList.remove('active');
                //     }
                //   });
                // });
            },

            { passive: true },
        );
    }, []);

    return (
        <div>
            <div className='scroll-up-btn' id='scroll-up-btn'>
                <a href='#home' style={{ color: 'white' }}>
                    <FontAwesomeIcon icon={faAngleUp} />
                </a>
            </div>

            <div className='header'>
                <nav className='header-nav' id='navbar'>
                    <div className='max-width'>
                        <input type='checkbox' id='check'></input>
                        <label for='check' className='checkbtn'>
                            <FontAwesomeIcon icon={faBars} />
                        </label>
                        <label className='brand'>
                            <a href='#home' className='brand'>
                                hung <span>dev.</span>
                            </a>
                        </label>
                        <ul className='menu'>
                            <li className='menu-item'>
                                <a className='menu-item-link active' href='#home'>
                                    Home
                                </a>
                            </li>
                            <li className='menu-item'>
                                <a className='menu-item-link' href='#about'>
                                    About
                                </a>
                            </li>
                            <li className='menu-item'>
                                <a className='menu-item-link' href='#services'>
                                    Services
                                </a>
                            </li>
                            <li className='menu-item'>
                                <a className='menu-item-link' href='#skills'>
                                    Skills
                                </a>
                            </li>
                            <li className='menu-item'>
                                <a className='menu-item-link' href='#projects'>
                                    Projects
                                </a>
                            </li>
                            <li className='menu-item'>
                                <a className='menu-item-link' href='#contact'>
                                    Contact
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <Home />
            <About />
            <Service />
            <Skill />
            <Project />
            <Contact />

            <footer>
                <span>
                    Created By{' '}
                    <a href='https://www.facebook.com/Kendz256/'>Hưng</a> |{' '}
                    <span className='far fa-copyright'>
                        <FontAwesomeIcon icon={faCopyright} />
                    </span>{' '}
                    2021 All rights reserved.
                </span>
            </footer>
        </div>
    );
}

export default App;
