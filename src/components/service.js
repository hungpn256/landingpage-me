import {
  faChartLine,
  faCode,
  faPaintBrush,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import vanilla from 'vanilla-tilt';
const Service = () => {
  useEffect(() => {
    const cards = document.querySelectorAll('.services .card');
    cards.forEach((card) => {
      vanilla.init(card, { scale: 1.1 });
    });
  });
  return (
    <section className='services' id='services'>
      <div
        className='max-width'
        data-aos='fade-up'
        data-aos-anchor-placement='bottom-bottom'
      >
        <h2 className='title'>My services</h2>
        <div className='serv-content'>
          <div className='card'>
            <div className='box'>
              {/* <FontAwesomeIcon className='icon' icon={faPaintBrush} /> */}
              <FontAwesomeIcon className='icon' icon={faCode} />
              <div className='text'>Front-end</div>
              <p>
                React, React-redux, redux-saga, dvajs, firebase...
              </p>
            </div>
          </div>
          <div className='card'>
            <div className='box'>
              <FontAwesomeIcon className='icon' icon={faCode} />
              <div className='text'>Back-end</div>
              <p>CRUD express, mongodb, back-end cơ bản</p>
            </div>
          </div>
          <div className='card'>
            <div className='box'>
              <FontAwesomeIcon className='icon' icon={faChartLine} />
              <div className='text'>Giải trí</div>
              <p>Đàn guitar, ca hát, đá bóng, chơi game, ...</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Service;
