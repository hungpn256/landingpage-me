import React from 'react';

export default function Skill() {
  return (
    <section className='skills' id='skills'>
      <div className='max-width' data-aos='zoom-out'>
        <h2 className='title'>My skills</h2>
        <div className='skills-content'>
          <div className='column left'>
            <div className='text'>Định hướng:</div>
            <p>
              Học thành thạo các kỹ năng hiện có, trở thành 1
              fullstack developer, làm việc cùng các chuyên gia trong
              ngành, ngoài ra học thêm các kỹ năng mềm: giao tiếp, làm
              việc nhóm, thuyết trình...
            </p>
            <a href='#home'>Read more</a>
          </div>
          <div className='column right'>
            <div className='bars'>
              <div className='info'>
                <span>HTML,CSS</span>
                <span>90%</span>
              </div>
              <div className='line html'></div>
            </div>
            <div className='bars'>
              <div className='info'>
                <span>JavaScript</span>
                <span>80%</span>
              </div>
              <div className='line js'></div>
            </div>
            <div className='bars'>
              <div className='info'>
                <span>ReactJs</span>
                <span>60%</span>
              </div>
              <div className='line css'></div>
            </div>
            <div className='bars'>
              <div className='info'>
                <span>Nodejs</span>
                <span>30%</span>
              </div>
              <div className='line nodejs'></div>
            </div>
            <div className='bars'>
              <div className='info'>
                <span>Git</span>
                <span>50%</span>
              </div>
              <div className='line mongodb'></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
