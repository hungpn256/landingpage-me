import React from 'react';
import Typed from 'react-typed';
export default function Home() {
  const strings = ['Hung', 'Front-end Developer'];
  return (
    <section className='home' id='home'>
      <div className='max-width'>
        <div className='home-content'>
          <div className='text-1'>Hello, my name is</div>
          <div className='text-2'>Hung</div>
          <div className='text-3'>
            And I'm a{' '}
            <Typed
              strings={strings}
              typeSpeed={100}
              backSpeed={40}
              loop={true}
            ></Typed>
          </div>
          <a href='#home'>Hire me</a>
        </div>
      </div>
    </section>
  );
}
