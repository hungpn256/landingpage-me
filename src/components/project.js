import React from 'react';
import imgEEcommerce from '../assets/e-ecommerce.png';
import imgAIS from '../assets/landingAIS.png';
import imgHDT from '../assets/landingHDT.png';
import SwiperCore, {
  A11y,
  Navigation,
  Pagination,
  Scrollbar,
  Zoom,
} from 'swiper';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/swiper.scss';
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Zoom]);
const Project = () => {
  return (
    <section className='projects' id='projects'>
      <div className='max-width'>
        <h2 className='title' style={{ zIndex: 0 }}>
          My Projects
        </h2>
        <Swiper
          spaceBetween={30}
          slidesPerView={1}
          navigation
          pagination={{ clickable: true }}
          autoHeight={true}
        >
          <SwiperSlide className='card' zoom>
            <div className='box'>
              <a href='https://landing-page-aisoft.vercel.app/'>
                <img src={imgAIS} alt='abc' />
              </a>
              <div className='description'>
                <div className='text'>Landing page AISoft</div>
                <p>
                  Web Landing AIS responsive đầy đủ tất cả các loại
                  màn hình
                </p>
                <a href='https://landing-page-aisoft.vercel.app/'>
                  Ghé thăm
                </a>
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide className='card' zoom>
            <div className='box'>
              <a href='https://e-ecommerce-hungpn256.vercel.app/'>
                <img src={imgEEcommerce} alt='abc' />
              </a>
              <div className='description'>
                <div className='text'>E-Ecommerce</div>
                <p>
                  Web bán hàng với các chức năng: Đăng nhập,đăng
                  xuất,đăng ký,tìm kiếm,phân trang, và dần phát triển
                  thêm nhiều chức năng khác nữa...
                </p>
                <a href='https://e-ecommerce-hungpn256.vercel.app/'>
                  Ghé thăm
                </a>
                {/* <div class='btn'>
                  <a href='https://e-ecommerce-hungpn256.vercel.app/'>
                    <span>Ghé thăm</span>
                  </a>
                </div> */}
              </div>
            </div>
          </SwiperSlide>
          <SwiperSlide className='card' zoom>
            <div className='box'>
              <a href='https://frontend-hungpn256.vercel.app/'>
                <img src={imgHDT} alt='abc' />
              </a>
              <div className='description'>
                <div className='text'>
                  CMS landing page Hợp Đại Thành
                </div>
                <p>
                  Web landing + CMS strapi có thể quản lý content,
                  thay đổi content trong web
                </p>
                <a href='https://frontend-tau-ten.vercel.app/'>
                  Ghé thăm
                </a>
                {/* <div class='btn'>
                  <a href='https://frontend-hungpn256.vercel.app/'>
                    <span>Ghé thăm</span>
                  </a>
                </div> */}
              </div>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </section>
  );
};

export default Project;
