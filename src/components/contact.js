import {
  faEnvelope,
  faMapMarkerAlt,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
const Contact = () => {
  return (
    <section className='contact' id='contact'>
      <div
        className='max-width'
        data-aos='fade-up'
        data-aos-duration='5000'
      >
        <h2 className='title'>Contact me</h2>
        <div className='contact-content'>
          <div className='column left'>
            <div className='text'>Get in Touch</div>
            <p>
              Dưới đây là một số thông tin có thể liên hệ với tôi,
              ngoài ra bạn có thể liên hệ với tôi qua form bên cạnh
            </p>
            <div className='icons'>
              <div className='row'>
                <FontAwesomeIcon className='icon' icon={faUser} />
                <div className='info'>
                  <div className='head'>Name</div>
                  <div className='sub-title'>Phạm Năng Hưng</div>
                </div>
              </div>
              <div className='row'>
                <FontAwesomeIcon
                  className='icon'
                  icon={faMapMarkerAlt}
                />
                <div className='info'>
                  <div className='head'>Address</div>
                  <div className='sub-title'>Hà Đông, Hà Nội</div>
                </div>
              </div>
              <div className='row'>
                <FontAwesomeIcon className='icon' icon={faEnvelope} />
                <div className='info'>
                  <div className='head'>Email</div>
                  <div className='sub-title'>
                    phamnanghung.25@gmail.com
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='column right'>
            <div className='text'>Message me</div>
            <form action='#'>
              <div className='fields'>
                <div className='field name'>
                  <input type='text' placeholder='Name' required />
                </div>
                <div className='field email'>
                  <input type='email' placeholder='Email' required />
                </div>
              </div>
              <div className='field'>
                <input type='text' placeholder='Subject' required />
              </div>
              <div className='field textarea'>
                <textarea
                  cols='30'
                  rows='10'
                  placeholder='Message..'
                  required
                ></textarea>
              </div>
              <div className='button'>
                <button type='submit'>Send message</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
