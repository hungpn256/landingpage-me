import React from 'react';

import avatarImg from '../assets/avatar.jpg';
const About = () => {
  return (
    <section className='about' id='about'>
      <div className='max-width'>
        <h2 className='title'>About me</h2>
        <div className='about-content'>
          <div data-aos='fade-right'>
            <div className='column left'>
              <img src={avatarImg} alt='abc' />
            </div>
          </div>
          <div className='column right'>
            <div data-aos='fade-left'>
              <div className='text'>
                I'm <span className='typing-2'>Hung</span> and I'm a{' '}
                <span className='typing-2'>Front-end Deverloper</span>
              </div>
              <p>
                Trường: PTIT, hiện tại là sinh viên năm 3
                <br />
                Chuyên ngành: CNTT
                <br />
                GPA: 3.09
                <br />
                Giải thưởng: tiềm năng ACM CLB, Trường
                <br />
                Chứng chỉ: Thuật toán samsung
                <br />
                Kinh nghiệm: 6 tháng đi làm Reactjs
                <br />
                Sở thích: code, chơi game, chơi đàn, nghe nhạc
                <br />
                Địa chỉ: ngõ 92 đường Trần Phú, Hà Đông, Hà Nội
                <br />
                Số điện thoại: 0868732357
                <br />
                Gmail: phamnanghung.25@gmail.com
                <br />
                Facebook:{' '}
                <a href={'https://www.facebook.com/Kendz256/'}>
                  https://www.facebook.com/Kendz256/
                </a>
                <br />
                Instagram:
                <a href={'https://www.instagram.com/hungpn256/'}>
                  https://www.instagram.com/hungpn256/
                </a>
              </p>
              <a className='dowloadCV' href='#home'>
                Download CV
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
